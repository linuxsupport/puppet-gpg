class gpg {

  package { 'gnupg2':
    ensure => present,
  }

  ->

  file { '/etc/gnupg/gpgconf.conf':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0600',
    source => 'puppet:///modules/gpg/gpgconf.conf'
  }

}
    	
