Name:           puppet-gpg
Version:        2.0
Release:        1%{?dist}
Summary:        Masterless puppet module for gpg

Group:          CERN/Utilities
License:        BSD
URL:            http://linux.cern.ch
Source0:        %{name}-%{version}.tar.gz

BuildArch:      noarch

%description
Masterless puppet module for fixing gpg issues in CC7

%prep
%setup -n code

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/gpg/
cp -ra * %{buildroot}/%{_datadir}/puppet/modules/gpg/
touch %{buildroot}/%{_datadir}/puppet/modules/gpg/linuxsupport

%files -n puppet-gpg
%{_datadir}/puppet/modules/gpg

%doc README.md

%changelog
* Thu May 03 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0-1
- Rebuild for 7.5 release

* Tue Nov 29 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.2-1
- Rebuild for 7.3 release

* Mon Nov 21 2016 Aris Boutselis <aris.boutselis@cern.ch>
- Initial release
